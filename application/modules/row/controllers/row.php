<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Row extends Frontend_Controller
{ 

	public function __construct(){
		parent::__construct();
		
		$this->modules_m = $this->load->Pmodel('modules_m');
	} 
	
	public function index($id = ''){
			
		$module = $this->modules_m->get($id);
		
		$css = getCss($module, 'row');
		echo $css;		
	}
}