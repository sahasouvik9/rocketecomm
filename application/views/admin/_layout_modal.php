<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

?>
<?php $this->load->view('admin/components/page_head'); ?>

<?php $this->load->view($subview); // Subview is set in controller ?>

<?php $this->load->view('admin/components/page_footer'); ?>