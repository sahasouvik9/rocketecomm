<!DOCTYPE html>
<html lang="en">
<head>
	<title><?php echo $title; ?></title>		
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=1, minimum-scale=0.5, maximum-scale=1.0"/>
	<meta content="<?php echo $meta_description; ?>" name="description" />
	<meta content="<?php echo $meta_keywords; ?>" name="keywords" />
	<?php echo $meta; ?>
	<?php echo $this->output->assets(); ?>
	<link rel="shortcut icon" href="<?php echo base_url('media/assets/icon.png'); ?>" />
	<link href="http://fonts.googleapis.com/css?family=Oswald%7CPT+Sans%7COpen+Sans" rel="stylesheet" type="text/css"/>
	<link type="text/css" href="<?php echo base_url('application/views/themes/default/css/font-awesome-ie7.min.css'); ?>" rel="stylesheet" media="all" />
	<link type="text/css" href="<?php echo base_url('application/views/themes/default/css/font-awesome.min.css'); ?>" rel="stylesheet" media="all" />
	<link type="text/css" href="<?php echo base_url('application/views/themes/default/css/template.css'); ?>" rel="stylesheet" media="all" />
	
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
 <body>	
        <header>
          <div class="head-top">
 	     <div class="container-fluid">
 	        <a href="javascript:void(0)" class="add_item_mydesign"><button class="dbtn"><label>1</label><span>Import Your Artwork</span></button></a>
                <a href="javascript:void(0)" title="" data-toggle="modal" data-target="#dg-myclipart"><button class="dbtn"><label>2</label><span>Upload Image</span></button></a>
                <a href="javascript:void(0)" class="add_item_clipart active" title="" data-toggle="modal" data-target="#dg-cliparts"><button class="dbtn"><label>3</label><span>Clipart Library</span></button></a>
                <a href="javascript:void(0)" onclick="design.save()" title="Save"><button class="dbtn"><label>4</label><span>Save Your Design</span></button></a>
 	     </div>
 	  </div>	 
          <div class="head-bott">
    	    <div class="container-fluid">
  	      <div class="col-xs-12 col-sm-6 col-md-8">
   	      <div class="doc_info"><span>Current Size: </span><div class="valueb"><select id="design_size"><option value="2-4">2x4</option><option value="2-20">2x20</option></select></div></div>
    	      <div class="doc_info"><span>Current Grade: </span><div class="value">Premium</div></div>
    	      <div class="doc_info"><span>Price Each: </span><div class="value">$84.50</div></div>
  	   </div>
        </header>
        
            
	<?php echo $subview;?>
		
	<!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="js/bootstrap.min.js"></script>
  </body>
</html>